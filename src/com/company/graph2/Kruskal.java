package com.company.graph2;

public class Kruskal {
    int numberOfVertexes;
    int[] parent;

    public Kruskal(int numberOfVertexes) {
        this.numberOfVertexes =numberOfVertexes;
        this.parent=new int [numberOfVertexes];
    }

    int find(int i) {
        while (this.parent[i] != i)
            i = this.parent[i];
        return i;
    }

    void union(int i, int j) {
        int a = find(i);
        int b = find(j);
        parent[a] = b;
    }

    int Kruskal(int matrix[][]) {
        int mincost = 0;
        for (int i = 0; i < numberOfVertexes; i++) {
            parent[i] = i;
        }
        int edge_count = 0;
        while (edge_count < numberOfVertexes - 1) {
            int min = Integer.MAX_VALUE;
            int a = -1, b = -1;
            for (int i = 0; i < numberOfVertexes; i++) {
                for (int j = 0; j < numberOfVertexes; j++) {
                    if (find(i) != find(j) && matrix[i][j] < min && matrix[i][j]!=0) {
                        min = matrix[i][j];
                        a = i;
                        b = j;
                    }
                }
            }
            union(a, b);
            edge_count++;
            mincost += min;
        }
        return mincost;
    }
}
