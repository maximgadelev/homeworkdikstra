package com.company.graph2;

import java.util.ArrayDeque;
import java.util.HashMap;

public class GraphTasks2Solution implements GraphTasks2{
    @Override
    public HashMap<Integer, Integer> dijkstraSearch(int[][] adjacencyMatrix, int startIndex) {
        int length = adjacencyMatrix.length;
        ArrayDeque<Integer> vertexDeque = new ArrayDeque<>();
        HashMap<Integer,Integer> dijkstraTable = new HashMap<>();
        HashMap<Integer,Integer> dijkstraTable2 = new HashMap<>();
        vertexDeque.add(startIndex);
        dijkstraTable.put(startIndex, 0);
        for (int i = 1; i < length + 1; i++) {
            int sv = vertexDeque.size();
            for (int j = 0; j < sv; j++) {
                dijkstraTable2.put(vertexDeque.getFirst(), dijkstraTable.get(vertexDeque.getFirst()));
                vertexDeque.removeFirst();
            }
            for (int j = 0; j < sv; j++) {
                vertexDeque.push(dijkstraTable2.entrySet().stream().max((entry1, entry2) -> entry1.getValue() > entry2.getValue() ? 1 : -1).get().getKey());
                dijkstraTable2.remove(vertexDeque.getFirst());
            }
            startIndex = vertexDeque.getFirst();
            for (int j = 1; j < length + 1; j++) {
                if (adjacencyMatrix[startIndex - 1][j - 1] > 0) {
                    if (!dijkstraTable.containsKey(j)) {
                        vertexDeque.add(j);
                        dijkstraTable.put(j, dijkstraTable.get(startIndex) + adjacencyMatrix[startIndex - 1][j - 1]);
                    }
                    if (dijkstraTable.containsKey(j) && dijkstraTable.get(j) > dijkstraTable.get(startIndex) + adjacencyMatrix[startIndex - 1][j - 1]) {
                        dijkstraTable.put(j, dijkstraTable.get(startIndex) + adjacencyMatrix[startIndex - 1][j - 1]);
                    }
                }
            }
            vertexDeque.removeFirst();
        }
        return dijkstraTable;
    }

    @Override
    public Integer primaAlgorithm(int[][] adjacencyMatrix) {
        int numberOfVertexes = adjacencyMatrix.length;
        int edgesInTheOstov = 0;
        boolean[] isUsedVertexes= new boolean[numberOfVertexes];
        isUsedVertexes[0] = true;
        int weight = 0;
        while (edgesInTheOstov < numberOfVertexes - 1) {
            int min = Integer.MAX_VALUE;
            int rowNumber = 0;
            int colNumber = 0;
            for (int i = 0; i < numberOfVertexes; i++) {
                if (isUsedVertexes[i] == true) {
                    for (int j = 0; j < numberOfVertexes; j++) {
                        if (!isUsedVertexes[j] && adjacencyMatrix[i][j] != 0) {
                            if (min > adjacencyMatrix[i][j]) {
                                min = adjacencyMatrix[i][j];
                                rowNumber = i;
                                colNumber = j;
                            }
                        }
                    }
                }
            }
            weight +=adjacencyMatrix[rowNumber][colNumber];
            isUsedVertexes[colNumber] = true;
            edgesInTheOstov++;
        }
        return weight;
    }

    @Override
    public Integer kraskalAlgorithm(int[][] adjacencyMatrix) {
        Kruskal kruskal = new Kruskal(adjacencyMatrix.length);
        int result=kruskal.Kruskal(adjacencyMatrix);
        return result;
    }
}
